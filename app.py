from flask import Flask, render_template, request
from faker import Faker
import requests

fake = Faker()

app = Flask(__name__)


@app.route('/space/')
def space():
    r = requests.get('http://api.open-notify.org/astros.json')
    cosmonauts = r.json()

    return render_template(
        'space.html',
        cosmonauts=cosmonauts['people']
    )


@app.route('/generate-users/', methods=['POST', 'GET'])
def generate_users():
    count = 100
    users = []

    if request.method == 'POST' and request.form['count'].isdigit():
        count = int(request.form['count'])

    for i in range(count):
        name = fake.unique.first_name()
        users.append(name + ' ' + name + "@email.com")

    return render_template(
        'generate-users.html',
        users=users
    )


@app.route('/requirements/')
def requirements():
    with open("requirements.txt", "r") as f:
        content = f.read()

    return render_template(
        'requirements.html',
        content=content
    )


if __name__ == '__main__':
    app.run(debug=True)
